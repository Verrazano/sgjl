#ifndef GAME_H_
#define GAME_H_

#include <cstdint>
#include <functional>
#include "Graphics.h"

class Game : public sf::RenderWindow
{
public:
	Game(std::string title, uint32_t width = 800,
		uint32_t height = 600, uint32_t framerate = 30)
	{
		create(sf::VideoMode(width, height), title, sf::Style::Close);
		setFramerateLimit(framerate);

		tick = render = post = [](){};

		for(uint32_t i = 0; i < sf::Keyboard::KeyCount; i++)
		{
			newPresses.push_back(false);
			oldPresses.push_back(false);

		}

		srand(time(NULL));

	}

	void run()
	{
		while(isOpen())
		{
			sf::Event event;
			while(pollEvent(event))
			{
				if(event.type == sf::Event::Closed)
				{
					close();

				}

			}

			updateKeys();

			tick();

			clear(sf::Color::White);

			render();

			display();

			post();

		}

	}

	bool pressing(sf::Keyboard::Key key)
	{
		return newPresses[key];

	}

	bool justPressed(sf::Keyboard::Key key)
	{
		return newPresses[key] && !oldPresses[key];

	}

	bool justReleased(sf::Keyboard::Key key)
	{
		return !newPresses[key] && oldPresses[key];

	}

	std::function<void(void)> tick;
	std::function<void(void)> render;
	std::function<void(void)> post;

private:
	void updateKeys()
	{
		for(uint32_t i = 0; i < newPresses.size(); i++)
		{
			oldPresses[i] = newPresses[i];
			newPresses[i] = sf::Keyboard::isKeyPressed((sf::Keyboard::Key)i);

		}

	}

	std::vector<bool> newPresses;
	std::vector<bool> oldPresses;

};

class Simple : public sf::Drawable
{
public:
	Simple(float width, float height)
	{
		setSize(width, height);
		body.setFillColor(sf::Color::Transparent);
		body.setOutlineColor(sf::Color::Transparent);
		body.setOutlineThickness(1);

	}

	Simple()
	{
		body.setFillColor(sf::Color::Transparent);
		body.setOutlineColor(sf::Color::Transparent);
		body.setOutlineThickness(1);

	}

	void setPosition(float x, float y)
	{
		sprite.setPosition(x, y);
		body.setPosition(x, y);

	}

	sf::Vector2f getPosition()
	{
		return body.getPosition();

	}

	void move(float x, float y)
	{
		sf::Vector2f pos = getPosition() + sf::Vector2f(x, y);
		setPosition(pos.x, pos.y);

	}

	void setSize(float width, float height)
	{
		body.setSize(sf::Vector2f(width, height));
		body.setOrigin(width/2, height/2);

	}

	void setDebugColor(sf::Color color)
	{
		body.setOutlineColor(color);

	}

	bool intersects(Simple& other)
	{
		return body.getGlobalBounds().intersects(other.body.getGlobalBounds());

	}

	bool intersects(Simple& other, sf::FloatRect& rect)
	{
		return body.getGlobalBounds().intersects(other.body.getGlobalBounds(), rect);

	}

	bool overlaps(Simple& other)
	{
		return sprite.intersects(other.sprite);

	}

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(sprite, states);
		target.draw(body, states);

	}

	Sprite sprite;

protected:
	sf::RectangleShape body;

};

class Group : public sf::Drawable
{
public:
	Group()
	{
		doing = false;

	}

	void add(float x, float y)
	{
		type.setPosition(x, y);
		toAdd.push_back(type);
		update();

	}

	void add(Simple s)
	{
		toAdd.push_back(s);
		update();

	}

	void remove(Simple& s)
	{
		toRemove.push_back(&s);
		update();

	}

	void foreach(std::function<void(Simple&)> func)
	{
		doing = true;
		for(int i = 0; i < objects.size(); i++)
		{
			func(objects[i]);
			if(!doing)
			{
				update();
				return;

			}

		}
		doing = false;
		update();

	}

	void end()
	{
		doing = false;

	}

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		for(int i = 0; i < objects.size(); i++)
		{
			target.draw(objects[i], states);

		}

	}


	Simple type;
private:
	void update()
	{
		if(!doing)
		{
			for(int i = 0; i < toRemove.size(); i++)
			{
				for(int j = 0; j < objects.size(); j++)
				{
					if(toRemove[i] == &objects[j])
					{
						objects.erase(objects.begin()+j);
						break;

					}

				}

			}

			toRemove.clear();

			for(int i = 0; i < toAdd.size(); i++)
			{
				objects.push_back(toAdd[i]);

			}

			toAdd.clear();

		}

	}

	bool doing;

	std::vector<Simple*> toRemove;
	std::vector<Simple> toAdd;
	std::vector<Simple> objects;

};

#endif /*GAME*/