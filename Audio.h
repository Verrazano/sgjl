#ifndef AUDIO_H_
#define AUDIO_H_

#include <map>
#include <string>
#include <cstddef>
#include <SFML/Audio.hpp>

class SoundEffect : public sf::Sound
{
public:
	SoundEffect(std::string path)
	{	
		static std::map<std::string, sf::SoundBuffer*> buffers;
		auto it = buffers.find(path);
		if(it != buffers.end())
		{
			auto buffer = it->second;
			if(buffer)
			{
				setBuffer(*buffer);

			}

		}
		else
		{
			sf::SoundBuffer* buffer = new sf::SoundBuffer();
			if(buffer->loadFromFile(path))
			{
				buffers.emplace(path, buffer);
				setBuffer(*buffer);

			}
			else
			{
				delete buffer;
				buffers.emplace(path, nullptr);

			}


		}

	}

};

#endif /*AUDIO*/