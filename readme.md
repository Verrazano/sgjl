# Simple Game Jam Library
Simple Game Jam Library is an extension of SFML
designed to provide simpler and quicker interfaces
for designing objects and doing general game type stuff.

SGJL isn't meant to perfect or necessarially pretty
it is mostly meant to be quick for developing any general
game quickly for game jam competitions such as Ludum Dare
or 1 Hour Game Jam.

## Features

### SoundEffect class

In SFML playing a sound can be tedious:
```
sf::SoundBuffer buffer;
buffer.loadFromFile("tada.wav");

sf::Sound sound;
sound.setBuffer(buffer);

sound.play();
```

Using the SGJL SoundEffect class:
```
SoundEffect sound("tada.wav");
sound.play();
```

### Sprite class

Not to be confused with sf::Sprite the SGJL Sprite is
an extension of sf::RectangleShape.

 * Provides an override for setSize that takes two floats.
 * setTexture using a path.
 * Functions to flip horizontally and vertically.
 * Provides a version of intersects that doesn't require getGlobalBounds()

```
Sprite tank(100, 100);
tank.setTexture("tank.png");
.
.
.
window.draw(tank);
```

### Simple class

The Simple class is a simple game object
it contains a Sprite object but also has sf::RectangleShape
member called body.

The body is used as a bounds for
the object that can differ from the sprite size
it's outline can be colored for use in debugging.

This is primarly a quick way to get some useful
functionality for example creating two game objects
and checking if they collide.
It is a good starting point to prototype off of.