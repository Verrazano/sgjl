//g++ main.cpp -o pong -std=c++11 -lsfml-graphics -lsfml-window -lsfml-system

#include "../SGJL.h"
#include <iostream>

const bool DEBUG = true;

sf::Vector2f randomVariance(sf::Vector2f a)
{
	float angle = angleOf(a);

	angle += qrandIn(-10.0f, 10.0f);
	angle = (int)angle;
	float len = mag(a);

	sf::Vector2f v = len*unitVec(toRad(angle));

	return v;

}

int main(int argc, char** argv)
{
	Game pong("pong");

	float speed = 10;

	Sprite court;
	court.setTexture("court.png");
	court.setPosition(400, 300);

	Simple left(32, 128);
	left.setPosition(64, 300);
	left.sprite.setTexture("paddle.png");

	Simple right(32, 128);
	right.setPosition(800 - 64, 300);
	right.sprite.setTexture("paddle.png");

	Simple ball(32, 32);
	ball.setPosition(400, 300);
	ball.sprite.setTexture("ball.png");

	std::vector<sf::Vector2f> startingVel;
	for(uint32_t i = 0; i < 4; i++)
	{
		startingVel.push_back(speed*unitVec(toRad(45 + 90*i)));

	}

	sf::Vector2f vel = pickRand(startingVel);

	if(DEBUG)
	{
		left.setDebugColor(sf::Color::Red);
		right.setDebugColor(sf::Color::Red);
		ball.setDebugColor(sf::Color::Red);

	}

	pong.tick = [&]()
	{
		//Handle moving paddles
		if(pong.pressing(sf::Keyboard::W)
			&& left.getPosition().y > 64)
		{
			left.move(0, -speed);

		}

		if(pong.pressing(sf::Keyboard::S)
			&& left.getPosition().y < 536)
		{
			left.move(0, speed);

		}

		if(pong.pressing(sf::Keyboard::Up)
			&& right.getPosition().y > 64)
		{
			right.move(0, -speed);

		}

		if(pong.pressing(sf::Keyboard::Down)
			&& right.getPosition().y < 536)
		{
			right.move(0, speed);

		}

		//Handle ball
		ball.move(vel.x, vel.y);

		//TODO: add a small random variance
		sf::Vector2f pos = ball.getPosition();
		if(pos.x < 16 || pos.x > 800 - 16
			|| ball.intersects(left) || ball.intersects(right))
		{
			vel.x *= -1;
			vel = randomVariance(vel);
			speed++;
			vel = speed*unitVec(vel);

		}
		
		if(pos.y < 16 || pos.y > 600 - 16)
		{
			vel.y *= -1;
			vel = randomVariance(vel);

		}

	};

	pong.render = [&]()
	{
		pong.draw(court);
		pong.draw(left);
		pong.draw(right);
		pong.draw(ball);

	};

	pong.run();	

	return 0;

}