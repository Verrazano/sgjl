#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <map>
#include <string>
#include <cstddef>
#include <SFML/Graphics.hpp>

class Sprite : public sf::RectangleShape
{
public:
	Sprite()
	{
	}

	void setSize(float width, float height)
	{
		sf::RectangleShape::setSize(sf::Vector2f(width, height));
		sf::RectangleShape::setOrigin(width/2, height/2);

	}

	void setTexture(std::string path, bool resetSize = true)
	{
		static std::map<std::string, sf::Texture*> textures;
		auto it = textures.find(path);
		if(it != textures.end())
		{
			auto texture = it->second;
			if(texture)
			{
				sf::RectangleShape::setTexture(texture);
				if(resetSize)
				{
					sf::Vector2f size(texture->getSize());
					setSize(size.x, size.y);

				}

			}

		}
		else
		{
			sf::Texture* texture = new sf::Texture();
			if(texture->loadFromFile(path))
			{
				textures.emplace(path, texture);
				sf::RectangleShape::setTexture(texture);
				if(resetSize)
				{
					sf::Vector2f size(texture->getSize());
					setSize(size.x, size.y);


				}

			}
			else
			{
				delete texture;
				textures.emplace(path, nullptr);

			}


		}

	}

	void setFacingRight(bool right)
	{
		float facing = right ? 1 : -1;
		sf::Vector2f scale = getScale();
		setScale(scale.x*facing, scale.y);

	}

	void setFacingUp(bool up)
	{
		float facing = up ? 1 : -1;
		sf::Vector2f scale = getScale();
		setScale(scale.x, scale.y*facing); 

	}

	bool intersects(Sprite& other)
	{
		return getGlobalBounds().intersects(other.getGlobalBounds());

	}

	bool intersects(Sprite& other, sf::FloatRect& rect)
	{
		return getGlobalBounds().intersects(other.getGlobalBounds(), rect);

	}

private:

};

class Text : public sf::Text
{
public:
	Text(const sf::String& str)
	{
		setString(str);
		setFillColor(sf::Color::Black);

		static sf::Font font;
		static bool loaded = false;

		if(!loaded)
		{
			static const unsigned char data[] =
			{
				#include "Consolas.h"

			};
			font.loadFromMemory(data, sizeof(data));
			loaded = true;

		}

		sf::Text::setFont(font);

	}

	void setFont(std::string path)
	{
		static std::map<std::string, sf::Font*> fonts;
		auto it = fonts.find(path);
		if(it != fonts.end())
		{
			auto font = it->second;
			if(font)
			{
				sf::Text::setFont(*font);

			}

		}
		else
		{
			sf::Font* font = new sf::Font();
			if(font->loadFromFile(path))
			{
				fonts.emplace(path, font);
				sf::Text::setFont(*font);

			}
			else
			{
				delete font;
				fonts.emplace(path, nullptr);

			}


		}

	}

};

#endif /*GRAPHICS*/