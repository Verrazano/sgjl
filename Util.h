#ifndef UTIL_H_
#define UTIL_H_

#include <SFML/Graphics.hpp>
#include <math.h>
#include <limits.h>
#include <sstream>

typedef unsigned long ulong;

float pi = 3.14159f;

float toDeg(float r)
{
    float d = r*180.0f/pi;

    return d;

}

float toRad(float d)
{
    float r = d/180.0f*pi;
    return r;

}

float dist(sf::Vector2f a, sf::Vector2f b)
{
	sf::Vector2f d = b - a;
	float f = sqrt(pow(d.x, 2) + pow(d.y, 2));
	return f;

}

float mag(sf::Vector2f a)
{
	return sqrt(pow(a.x, 2) + pow(a.y, 2));

}

sf::Vector2f unitVec(sf::Vector2f a)
{
	float m = mag(a);

	sf::Vector2f f(a.x == 0 || m == 0 ? 0 : a.x/m,
		a.y == 0 || m == 0 ? 0 : a.y/m);
	return f;

}

sf::Vector2f unitVec(float r)
{
	sf::Vector2f vec(cos(r), sin(r));
	return vec;

}

sf::Vector2f randomUnitVec()
{
	float r = rand()%360;
	return unitVec(toRad(r));

}

float chop(float f, int x)
{
    if(x >= 5)
    {
        return f;

    }

	int z = (int)f; //integer part of the float
	if(x == 0)
	{
		return (float)z;
	}

	float s = pow(10.0f, (float)x); //amount of decimal places to keep
	f = (f - z)*s; //get the portion we want into the integer part of the number
	f = (int)f; //get rid of the decimals we don't want
	f = z + (f/s); //put the number back together

	return f;

}

ulong qrand()
{
	static ulong x=123456789;
	static ulong y=362436069;
	static ulong z=521288629;

	ulong t;

	x ^= x << 16;
	x ^= x >> 5;
	x ^= x << 1;

	t = x;
	x = y;
	y = z;
	z = t ^ x ^ y;

	return z;
	
}

float qrandIn(float min, float max)
{
	float range = (max - min);
	float rnd = min + (range*qrand())/(ULONG_MAX + 1.0f);
	return rnd;

}

float randIn(float min, float max)
{
	float range = (max - min);
	float rnd = min + (range*rand())/(RAND_MAX + 1.0f);
	return rnd;

}

template<typename T>
T& pickRand(std::vector<T>& vec)
{
	return vec[rand()%vec.size()];

} 

float angleTo(sf::Vector2f from, sf::Vector2f to)
{
    sf::Vector2f d = from - to;
    float r = atan2f(d.y, d.x) - pi; //flip because sfml y coordinates are flipped

    return r;

}

float angleOf(sf::Vector2f vec)
{
	float r = atan2f(vec.y, vec.x);
	if(r < 0.0f)
	{
		r += 2*pi;

	}

	return toDeg(r);

}

bool aprox(float a, float b, float tol)
{
	return fabs(a - b) <= tol;

}

float dot(sf::Vector2f a, sf::Vector2f b)
{
	return a.x*b.x + a.y*b.y;

}

template <typename T>
std::string toString(T& a)
{
	std::stringstream ss;
	ss << a;
	std::string str;
	ss >> str;
	return str;

}

int toInt(std::string str)
{
	std::stringstream ss;
	ss << str;
	int a;
	ss >> a;
	return a;

}

float toFloat(std::string str)
{
	std::stringstream ss;
	ss << str;
	float a;
	ss >> a;
	return a;

}

#endif /*UTIL*/